(ns ^:figwheel-always front.core
  (:require [om.core :as om :include-macros true]
            [om.dom :as d :include-macros true]
            [sablono.core :as html :refer-macros [html]]))


(enable-console-print!)


(defonce app-state (atom {:offers []
                          :next-id 0}))


(def app-history (atom [@app-state]))


(add-watch app-state :history
  (fn [_ _ _ n]
    (when-not (= (last @app-history) n)
      (swap! app-history conj n))))


(aset js/window "undo"
  (fn [e]
    (when (> (count @app-history) 1)
      (swap! app-history pop)
      (reset! app-state (last @app-history)))))


(defonce empty-offer {:company ""
                      :base 0
                      :equity-percent 0
                      :current-valuation 0
                      :expected-upside 1})


(defn create-new-offer [data]
  (om/transact! data :next-id #(inc %))
  (merge {:id (:next-id data)} empty-offer))


(defn update-offer [offer up-key up-value]
  (om/update! offer up-key up-value))


(defn delete-offer [data offer]
  (let [delete-id (:id offer)]
    (om/transact! data :offers #(filterv
                                  (fn [offer] (not (= delete-id (:id offer)))) %))))


(defn calc-total [offer]
  (+ (:base offer)
     (* 4
        (/ (:equity-percent offer) 100)
        (* (:current-valuation offer) (:expected-upside offer)))))


(defn add-new-offer [data]
  (let [new-offer (create-new-offer data)]
    (om/transact! data :offers #(conj % new-offer))))


(defn controls-view [data]
  (html
    [:fieldset
     [:label
      [:span "Add New Offer"]
      [:button {:type "button"
                :on-click #(add-new-offer data)}
       "Add"]]
     [:label
      [:span "Undo"]
      [:button {:type "button"
                :on-click #(.undo js/window)}
               "Undo"]]]))


(defn offer-view [data offer]
  (html
    [:tr
     [:td (:id offer)]
     [:td
      [:input {:type "text"
               :value (:company offer)
               :on-change #(update-offer offer :company (.. % -target -value))}]]
     [:td
      [:input {:type "number"
               :value (:base offer)
               :on-change #(update-offer offer :base (.. % -target -value))}]]
     [:td
      [:input {:type "number"
               :min 0
               :step 0.01
               :value (:equity-percent offer)
               :on-change #(update-offer offer :equity-percent (.. % -target -value))}]]
     [:td
      [:input {:type "number"
               :value (:current-valuation offer)
               :on-change #(update-offer offer :current-valuation (.. % -target -value))}]]
     [:td
      [:input {:type "number"
               :value (:expected-upside offer)
               :on-change #(update-offer offer :expected-upside (.. % -target -value))}]]
     [:td (calc-total offer)]
     [:td
      [:button {:type "button"
                :on-click #(delete-offer data offer)}
               "Delete"]]]))


(defn table-view [data]
  (html
    [:table
     [:caption "Offer Calculator"]
     [:thead
      [:tr
       [:th "Id"]
       [:th "Company"]
       [:th "Base (inc. bonus)"]
       [:th "Equity % (Vesting over 4 years)"]
       [:th "Current Valuation"]
       [:th "Expected Upside"]
       [:th "Total Comp Valuation (4 years)"]
       [:th "Delete Offer"]]]
     [:tbody
      (map (partial offer-view data) (:offers data))]]))


(defn form-view [data]
  (html
    [:form
      (controls-view data)
      [:fieldset
        (table-view data)]]))


(defn render-app [data owner]
  (reify
    om/IRender
      (render [this]
        (form-view data))))


(om/root render-app app-state {:target (. js/document (getElementById "app"))})

